# RKE automatic installation

## Goal

This project aims to automatically install a Kubernetes pod using IDF/PDF
description files from OPNFV and few variables. At the end of the deployment, it
can trigger a deployment of ONAP via OOM method.


## Automatic deployment

Today, this code is automatically deployed on Orange POD. You can look at
`.gitlab-ci.yml` to see how it's done.

## Usage

First thing is to provide good PDF/IDF files. Current files are for example but
allow you to deploy a 1 Controller / 4 Workers POD on OpenStack.
See OPNFV PDF/IDF description file to know how to map with your needs.

You also need access to a "jumphost" that will have access to the servers (
virtual or physical). This jumphost will be use as a ssh proxy. It's actually
not needed but not really tested (without jumphost) so PR are welcome :)

### on OpenStack

in order to deploy a pod on OpenStack, you must provide your credentials (in
`group_vars/all.yaml`). The jumphost will need to be able to create project,
servers, routers with these credentials.

Then you create a (simple) inventory file where you define how to access to your
jumphost (see http://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
for more information on how to define an inventory in ansible):

```
jumphost ansible_host=YOURJUMPHOST
```

Then you can launch the different steps:

```bash
# Machine creation with right stuff and inventory creation

# the key from key_name will be created in OpenStack if needed with the value
# found in private_key_path

# rke_path is a local path on the machine (not on jumphost)

# branch is used to name the project inside OpenStack
ansible-playbook -i inventory -e branch=my_branch -e key_name=MyKey -e private_key_path=~/.ssh/id_rsa -e rke_path=. opnfv-prepare.yaml

# Retrieval of rke and configuration of it

# put http_proxy and htts_proxy if needed

# rke_path is a local path on the machine (not on jumphost)

# branch is used to name the k8s cluster.
ansible-playbook -i artifacts/inventory -e branch=my_branch -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e rke_path=. opnfv-k8s-prepare.yaml


# launch rke
cd rke
ansible-playbook -i ../artifacts/inventory -b -e docker_dns_servers_strict=no cluster.yml
```

### on Baremetal


TODO
It's roughly the same as OpenStack except you need the machines to be started.

## Work to be done

- [ ] realign with IDF/PDF new format from opnfv
- [ ] realign with baremetal deployment work from Orange
- [ ] test if direct access to the machines (no jumphost) works.

## Work that may be done

- [ ] deploy on other cloud platform than OpenStack

## Input

  - configuration files:
    - mandatory:
        - vars/pdf.yml: POD Description File
        - vars/idf.yml: POD Installer Description File
        - inventory/infra: the ansible inventory for the servers
    - optional:
        - vars/vaulted_ssh_credentials.yml: Ciphered private/public pair of key
          that allows to connect to jumphost and servers
  - Environment variables:
    - mandatory:
        - PRIVATE_TOKEN: to get the artifact
        - artifacts_src: the url to get the artifacts
        - OR artifacts_bin: b64_encoded zipped artifacts (tbd)
        - ANSIBLE_VAULT_PASSWORD: the vault password needed by ciphered ansible
          vars
    - optional:
      - ANSIBLE_VERBOSE:
          - role: verbose option for ansible
          - values: "", "-vvv"
          - default: ""
      - ENABLE_MONITORING:
          - role: add monitoring of infrastructure via prometheus or not
          - value type: boolean
          - default: False
      - WEAVE_SCOPE_VERSION
          - role: if monitoring enabled, which version of weave scope to deploy
          - value type: string
          - default: "1.9.1"
      - docker_version:
          - role: the docker version (in package manager format)
          - default: "17.03.2~ce-0~debian-stretch"
      - kubernetes_release:
          - role: the helm version to deploy
          - default: v1.11.3
      - rke_version:
          - role: the helm version to deploy
          - default: v2.6.0
      - helm_version:
          - role: the helm version to deploy
          - default: v2.8.2
      - kube_network_plugin:
          - role: which network plugin to use
          - default: calico
      - use_jumphost:
          - role: do we need to connect via a jumphost or not?
          - value type: boolean
          - default: "yes"
      - proxy_command:
          - role: do we need to use a proxy command to reach the jumphost or
            not?
          - value: "", "the proxy command (example: connect -S socks:1080 %h
            %p)"
          - default: ""


## Output
  - artifacts:
    - vars/kube-config

## Monitoring

If monitoring is enabled, we first need a group `monitoring` with at least one
server. We'll install prometheus and grafana on this server and node_exporter to
all servers.
